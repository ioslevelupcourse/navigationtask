//
//  SixthViewController.m
//  NavigationTask
//
//  Created by Admin on 22.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "SixthViewController.h"

@interface SixthViewController ()

@end

@implementation SixthViewController

#pragma mark - Action
- (IBAction)goToRootVCButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
