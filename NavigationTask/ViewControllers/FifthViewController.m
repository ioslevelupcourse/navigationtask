//
//  FifthViewController.m
//  NavigationTask
//
//  Created by Admin on 22.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "FifthViewController.h"
#import "SixthViewController.h"

static NSString *const LUSixthTitle = @"Sixth";

@interface FifthViewController ()

@end

@implementation FifthViewController

#pragma mark - Action
- (IBAction)goToSixthVCbuttonClicked:(UIButton *)sender {
    SixthViewController *sixthViewController = [[UIStoryboard storyboardWithName:LUSecondStoryboard bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SixthViewController class])];
    sixthViewController.currentTitle = LUSixthTitle;
    [self.navigationController pushViewController:sixthViewController animated:YES];
}

@end
