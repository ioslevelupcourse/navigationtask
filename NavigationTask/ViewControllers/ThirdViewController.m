//
//  ThirdViewController.m
//  NavigationTask
//
//  Created by Admin on 22.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController


#pragma mark - Actions
- (IBAction)goToBackButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goToRootVCButtonClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
