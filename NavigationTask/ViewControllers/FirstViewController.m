//
//  FirstViewController.m
//  NavigationTask
//
//  Created by Admin on 22.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"
#import "SeventhViewController.h"
#import "FifthViewController.h"

#pragma mark - Init Constants
static NSString *const LUShowSecondViewController = @"ShowSecond";

static NSString *const LUTabOneTitleTabBar = @"TabOne";
static NSString *const LUFirstTitle = @"First";
static NSString *const LUSecondTitle = @"Second";
static NSString *const LUFifthTitle = @"Fifth";
static NSString *const LUSeventhTitle = @"Seventh";

@interface FirstViewController ()

@end

@implementation FirstViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = LUFirstTitle;
    [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:LUTabOneTitleTabBar];
}

#pragma mark - Actions
- (IBAction)goToXIBButtonClicked:(UIButton *)sender {
    SeventhViewController *seventhViewController = [SeventhViewController new];
    seventhViewController.currentTitle = LUSeventhTitle;
    [self.navigationController pushViewController:seventhViewController animated:YES];
}

- (IBAction)goToAnotherStoryboardButtonClicked:(UIButton *)sender {
    FifthViewController *fifthViewController = [[UIStoryboard storyboardWithName:LUSecondStoryboard bundle:nil] instantiateInitialViewController];
    fifthViewController.currentTitle = LUFifthTitle;
    [self.navigationController pushViewController:fifthViewController animated:YES];
}

#pragma mark - Privat
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:LUShowSecondViewController]) {
        SecondViewController *secondViewController = segue.destinationViewController;
        secondViewController.currentTitle = LUSecondTitle;
    }
}
@end
