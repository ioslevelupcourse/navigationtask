//
//  ModalViewController.m
//  NavigationTask
//
//  Created by Admin on 11.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "ModalViewController.h"

@interface ModalViewController ()

@end

@implementation ModalViewController

#pragma mark - Actions
- (IBAction)closeClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
