//
//  BaseViewController.h
//  NavigationTask
//
//  Created by Admin on 10.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - Public Constants
static NSString *const LUFirstStoryboard = @"First";
static NSString *const LUSecondStoryboard = @"Second";

@interface BaseViewController : UIViewController

@property(nonatomic, copy) NSString* currentTitle;

@end
