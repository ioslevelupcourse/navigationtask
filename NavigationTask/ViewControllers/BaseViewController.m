//
//  BaseViewController.m
//  NavigationTask
//
//  Created by Admin on 10.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "BaseViewController.h"


@interface BaseViewController ()

@end

@implementation BaseViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.currentTitle;
}

@end
