//
//  SecondViewController.m
//  NavigationTask
//
//  Created by Admin on 22.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "ModalViewController.h"

#pragma mark - Init Constants
static NSString *const LUShowThirdViewController = @"ShowTrird";
static NSString *const LUThirditle = @"Third";

@interface SecondViewController ()

@end

@implementation SecondViewController

#pragma mark - Actions
- (IBAction)modalBarButtonItemClick:(UIBarButtonItem *)sender {
    ModalViewController *modalViewController = [[UIStoryboard storyboardWithName:LUFirstStoryboard bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ModalViewController class])];
    modalViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    modalViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:modalViewController animated:YES completion:nil];
    
}

#pragma mark - Privat
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:LUShowThirdViewController]) {
        ThirdViewController *thirdViewController = segue.destinationViewController;
        thirdViewController.currentTitle = LUThirditle;
    }
}

@end
